package com.huskymvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HuskyMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuskyMvcApplication.class, args);
	}

}
