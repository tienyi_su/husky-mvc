package com.huskymvc.repository;

import com.huskymvc.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Tien-Yi Su
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long>
{
}
