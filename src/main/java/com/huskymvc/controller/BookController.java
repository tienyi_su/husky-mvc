package com.huskymvc.controller;

import com.huskymvc.entity.Book;
import com.huskymvc.repository.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author Tien-Yi Su
 */
@Controller
@RequestMapping("/books")
public class BookController
{
	private BookRepository repository;

	public BookController(BookRepository pRepository)
	{
		repository = pRepository;
	}

	@GetMapping("")
	public void retrieveBooks(Model pModel)
	{
		pModel.addAttribute("title", "The Hound of the Baskervilles");

		List<Book> books = repository.findAll();

		pModel.addAttribute("books", books);
	}
}
